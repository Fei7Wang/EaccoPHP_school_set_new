<?php

return [

    // 后台菜单及权限节点配置
    'admin_menus' =>[
        [
            'title'   =>'公司留言2',
            'name'    =>'DemoExample',
            'icon'    => '',
            'is_menu' => 1,
            //'sort'    => 3,
            'sub_menu'=>[
                [
                    'title'=>'留言表单Form',
                    'name' => 'home/plugin/execute?_plugin=DemoExample&_controller=BuilderForm&_action=Index',
                    'is_menu'=>1
                ],
                [
                    'title'=>'留言列表List',
                    'name' => 'home/plugin/execute?_plugin=DemoExample&_controller=BuilderList&_action=Index',
                    'is_menu'=>1
                ],
            ]
        ]
        
    ],
];