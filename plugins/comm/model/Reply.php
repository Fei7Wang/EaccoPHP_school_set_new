<?php
// 评论模型       

// +----------------------------------------------------------------------
namespace plugins\comm\model;

use app\common\model\Base;
use think\Model;

class Reply extends Model {
    protected $name='reply';
    protected $autoWriteTimestamp = false;
}