<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2018 http://www.eacoo123.com, All rights reserved.         
// +----------------------------------------------------------------------
// | [EacooPHP] 并不是自由软件,可免费使用,未经许可不能去掉EacooPHP相关版权。
// | 禁止在EacooPHP整体或任何部分基础上发展任何派生、修改或第三方版本用于重新分发
// +----------------------------------------------------------------------
// | Author:  心云间、凝听 <981248356@qq.com>
// +----------------------------------------------------------------------
namespace app\home\controller;
use app\home\model\Users;
use app\home\model\Focus;
class User extends Home {

    function _initialize()
    {
        parent::_initialize();

    }
        /*
    *    判断登录用户与当前用户是否有关注关系
    *    时间：2018年6月29日10:18:22
    *    作者： 岳晓飞
    *    0代表 未关注 1代表 已关注 -1代表 无关系
    */
    public function isFocus($uid_b)
    {
        if(is_login())
        {
            $uid_a=is_login();
        }
        else
        {
            return 0;
        }
        if($uid_a==$uid_b)
        {
            return -1;
        }else
        {
            $focusModel = new Focus();
            // 查询数据集
            $focus=$focusModel->where('uid_a',$uid_a)->limit(10)->order('id', 'desc')->select();
            foreach($focus as $f)
            {
                if($f->uid_b==$uid_b)
                {
                    return 1;
                }
            }
            return 0;
        }
    }
    
    /**
     * 首页
     * @return [type] [description]
     */
    public function personal($id)
    {
        $userModel=new Users();
        $user=$userModel->where('uid',$id)->find();
        $this->pageInfo('首页','home');

        $focusModel = new Focus();
        $focusInfo=$focusModel->where('uid_a',$id)->select();
        $fansInfo=$focusModel->where('uid_b',$id)->select();
        $focusCount=$focusModel->getFocusCount($id);
        $fansCount=$focusModel->getFansCount($id);

        foreach($focusInfo as $info)
        {
            $info['nickname']=$userModel->getInfo($info['uid_b'])['nickname'];
            $info['level']=$userModel->getInfo($info['uid_b'])['level'];    
            $info['fansCount']= $focusModel->getFansCount($info['uid_b']);     
            $info['focusCount']=$focusModel->getFocusCount($info['uid_b']);
            $info['description']=$userModel->getInfo($info['uid_b'])['description'];
            $info['avatar']=$userModel->getInfo($info['uid_b'])['avatar']; 
            $info['level']=$userModel->getInfo($info['uid_b'])['level']; 
        }
        foreach($fansInfo as $info)
        {
            $info['nickname']=$userModel->getInfo($info['uid_a'])['nickname'];
            $info['level']=$userModel->getInfo($info['uid_a'])['level'];    
            $info['fansCount']= $focusModel->getFansCount($info['uid_a']);     
            $info['focusCount']=$focusModel->getFocusCount($info['uid_a']);
            $info['description']=$userModel->getInfo($info['uid_a'])['description'];
            $info['avatar']=$userModel->getInfo($info['uid_a'])['avatar']; 
            $info['level']=$userModel->getInfo($info['uid_a'])['level']; 
            $info['combine']=$this->isFocus($info['uid_a']);

        }
    
        $this->assign('fansInfo',$fansInfo);
        $this->assign('focusInfo',$focusInfo);
        $this->assign('fansCount',$fansCount);
        $this->assign('focusCount',$focusCount);
        $this->assign('pageTitle',$user->nickname."的个人主页-圈子社区");
        $this->assign('user',$user);
        $this->assign('focusFlag',$this->isFocus($id)); //是否关注
        $this->assign('id',$id);



    	return $this->fetch();
    }
    /*
    *    判断登录用户与当前用户是否有关注关系
    *    时间：2018年6月29日10:18:22
    *    作者： 岳晓飞
    *    1代表 关注 0代表 取关
    */
    public function focus($id,$op)
    {
        $focusModel = new Focus();
        if(is_login()>0)
        {
            //如果登录
            $focusModel->uid_a = is_login();
        }else
        {
            $this->success('请先登录，正在跳转!','Index/index');
            return;
        }
       
        $focusModel->uid_b = $id;
        if($op==1)
        {         
            if($focusModel->save())
            {
                $this->success("关注成功，正在跳转!");
            }
            else{
                $this->error("关注失败，正在跳转!");
            }
        }
        else
        {
            $focus=$focusModel->where('uid_a',is_login())->where('uid_b',$id)->find();
            $idd=$focus['id'];
            $focus = Focus::get($idd);
            if($focus->delete()==1)
            {
                $this->success("取关成功，正在跳转!");
            }
            else
            {
                $this->error("取关失败，正在跳转!");
            }
        }
    }
}