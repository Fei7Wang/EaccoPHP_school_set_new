<?php
/**
 * 构建器Builder列表示例-后台
 */
namespace plugins\DemoExample\admin;

use app\admin\controller\Admin; 
use plugins\DemoExample\model\DemoExample as DemoExampleModel;

class BuilderList extends Admin{

    protected $plugin_name = 'DemoExample';

    function _initialize()
    {
        parent::_initialize();
        $this->demoExampleModel = new DemoExampleModel();
    }
    
    /**
     * 用户列表(构建器列表，该方法是以获取用户列表来举例)
     * @return [type] [description]
     * @date   2018-02-23
     * @author 心云间、凝听 <981248356@qq.com>
     */
    public function index(){
        // 获取构建器列表数据
        $map = [
            'status'=>['egt',0],// 禁用和正常状态
        ];
        list($data_list,$total) = $this->demoExampleModel->search('title')->getListByPage($map,true,'create_time desc');

        $tab_list = [
                'builderlist'=>['title'=>'列表示例','href'=>plugin_url('DemoExample/BuilderList/index')],
                'builderform'=>['title'=>'表单示例','href'=>plugin_url('DemoExample/BuilderForm/index')],
            ];
        return builder('List')
                ->setMetaTitle('DemoExample列表示例') // 设置页面标题
                ->setTabNav($tab_list, 'builderlist')  // 设置页面Tab导航
                ->addTopButton('addnew')  // 添加新增按钮
                ->addTopButton('resume')  // 添加启用按钮
                ->addTopButton('forbid')  // 添加禁用按钮
                ->addTopButton('delete')  // 添加删除按钮
                //->setSearch('custom','请输入关键字')
                ->keyListItem('id', 'ID')
                //->keyListItem('picture', '图像', 'picture')
                ->keyListItem('name', '公司名称')
                ->keyListItem('email', '公司邮箱')
                //->keyListItem('sex', '性别','array',[0=>'保密',1=>'男',2=>'女'])
                ->keyListItem('contacts', '联系人')
                ->keyListItem('phone', '联系电话')
                ->keyListItem('description', '公司说明')
                ->keyListItem('content', '招聘内容')
                ->keyListItem('file', '文件')
                ->keyListItem('create_time', '创建时间')
                ->keyListItem('status', '状态', 'status')
                ->keyListItem('right_button', '操作', 'btn')
                ->setListPrimaryKey('id')//设置数据主键，默认是id
                ->setListData($data_list)    // 数据列表
                ->setListPage($total) // 数据列表分页
                ->addRightButton('edit',['href'=>plugin_url('DemoExample/BuilderForm/index',['id'=>'__data_id__'])])
                //->addRightButton('forbid')
                //->addRightButton('delete')  // 添加编辑按钮
                ->fetch();
    }
    
    /**
     * 编辑
     * @return [type] [description]
     * @date   2018-03-08
     * @author 心云间、凝听 <981248356@qq.com>
     */
    public function edit()
    {
        $form = new BuilderForm();
        return $form->index();
    }
}
