<?php
namespace app\common\behavior;
class InitAction{
    public function run()
    {
        //$user=session('user_login_auth');
        $uid=is_login();
        if($uid){
            $time=time();
            model('common/User')->where('uid',$uid)->update(['last_active'=>$time]);
        }
    }
}