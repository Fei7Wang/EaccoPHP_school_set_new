<?php

return [

    // 后台菜单及权限节点配置
    'admin_menus' =>[
        [
            'title'   =>'后台工具',
            'name'    =>'admin/plugins/config?name=userTools',
            'icon'    => 'fa fa-cogs',
            'is_menu' => 1,
            'pid'     => 3,
        ]
        
    ],
];