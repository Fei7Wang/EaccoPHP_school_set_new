<?php
namespace plugins\userTools;
use app\common\controller\Plugin;
use app\common\model\User as UserModel;
/**
 * 图片轮播插件
 * @author birdy
 */
class Index extends Plugin {

    /**
     * @var array 插件钩子
     */
    public $hooks = [
        'AdminIndex'
    ];

    public function install(){
        return true;
    }

    public function uninstall(){
        //删除钩子
        return true;
    }
    
    //实现的后台首页钩子方法
    public function AdminIndex($param){
        $config = $this->getConfig();
        //最近用户
        $recently_users = UserModel::where('status',1)->field('uid,username,nickname,avatar,reg_time')->limit(12)->order('reg_time desc')->select();
        $this->assign('recently_users',$recently_users);

        //用户数据分析
        $grow_analyze = [
            'data'=>[10,26,21,35,43,45,50],
            'time'=>[
                date("m-d",strtotime("-7 day")),
                date("m-d",strtotime("-6 day")),
                date("m-d",strtotime("-5 day")),
                date("m-d",strtotime("-4 day")),
                date("m-d",strtotime("-3 day")),
                date("m-d",strtotime("-2 day")),
                date("m-d",strtotime("-1 day")),
                ]
            ];
        $this->assign('grow_analyze',json_encode($grow_analyze));
        echo $this->fetch('admin_index');
    }
}