<?php
namespace plugins\userEnhance;
use app\common\logic\User as UserLogic;
use app\common\controller\Plugin;
use app\common\model\User as UserModel;
class Index extends Plugin{
    /***
     * 注意，我的 uuninstall 文件故意命名错了
     * 因为当安装插件时使用‘清除’数据选项会自动运行 uninstall.sql 再运行 install.sql 
     * 但是有些字段不存在，会导致sql执行失败，安装失败
     * 所以 当你真正需要卸载的时候，自行把名称改回来
     */
    public $hooks = [
        'ShowLongIn',
        'ActiveLog',
        'LogOutLog'
    ];
    public function install(){
        return true;
    }

    public function uninstall(){
        //删除钩子
        return true;
    }

    public function ShowLongIn(){
        $list_login=UserLogic::getAllStates();
        $data = json_encode($list_login);
        $num=0;
        foreach ($list_login as $v) {
            if($v['login'])
                $num++;
        }
        $this->assign('data', $data);
        $this->assign('num',$num);
        return $this->fetch('login_list');
    }
    public function ActiveLog(){
        $user=session('user_login_auth');
        $uid=$user['uid'];
        $time=time();
        $modelUser= model('common/User');
        $modelUser->where('uid',$uid)->update('last_active',$time);
    }
    public function LogOutLog(){

    }
}