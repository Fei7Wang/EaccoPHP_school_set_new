
CREATE TABLE IF NOT EXISTS `eacoo_demo_example2` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '公司名称',
  `email` varchar(120) DEFAULT '' COMMENT '公司邮箱',
  `contacts` varchar(60) DEFAULT '' COMMENT '联系人',
  `phone` varchar(60) DEFAULT '' COMMENT '联系电话',
  `repeater_content` text NOT NULL COMMENT '通过repeater组件存储数据'
  `description` varchar(255) DEFAULT '' COMMENT '公司说明',
  `content` longtext COMMENT '招聘内容',
  `datetime` datetime NOT NULL COMMENT '时间',
  `file` varchar(120) NOT NULL DEFAULT '0' COMMENT '文件地址',
  `files` varchar(60) NOT NULL DEFAULT '' COMMENT '多个文件，id串存储形式',
  `region` varchar(255) NOT NULL DEFAULT '' COMMENT '地区组件数据存储',
  `sort` smallint(6) unsigned NOT NULL DEFAULT '99' COMMENT '排序',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态。0禁用，1正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='框架开发示例插件数据表';


INSERT INTO `eacoo_demo_example2` (`id`, `name`, `contacts`, `phone`, `description`, `content`, `datetime`, `file`, `files`, `region`, `sort`, `update_time`, `create_time`, `status`)
VALUES
	(1,'壹青年文化传媒有限责任公司','54158942@qq.com','李卫东','18013666990',0,4,'94,95,96','/logo.png','[{\"img\":\"94\",\"url\":\"http:\\/\\/www.eacoo123.com\",\"text\":\"EacooPHP\\u5feb\\u901f\\u5f00\\u53d1\\u6846\\u67b6\"},{\"img\":\"95\",\"url\":\"http:\\/\\/forum.eacoo123.com\",\"text\":\"EacooPHP\\u8ba8\\u8bba\\u793e\\u533a\"},{\"img\":\"94\",\"url\":\"http:\\/\\/www.eacoo123.com\",\"text\":\"EacooPHP\\u5feb\\u901f\\u5f00\\u53d1\\u6846\\u67b6\"}]','默认描述内容','<p></p><p>默认内容<br></p>','2018-03-02 16:38:35','/uploads/attachment/2016-07-27/579857b5aca95.mp3','10,12','{\"province\":\"1\",\"city\":\"2\",\"area\":\"83\"}',99,1519980119,1519978243,1);

