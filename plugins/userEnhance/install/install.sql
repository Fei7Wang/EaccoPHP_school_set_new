alter table eacoo_users modify birthday date default '1000-01-02' comment '生日';
alter table eacoo_users add school varchar(20)  comment '院系';
alter table eacoo_users add pro_title varchar(10)  comment '职称';
alter table eacoo_users add duty varchar(10)  comment '职务';
alter table eacoo_users add last_active int(10) comment '最后活动时间';
alter table eacoo_users add logout tinyint DEFAULT 0 comment '是否已经登出';