<?php

return [

    // 后台菜单及权限节点配置
    'admin_menus' =>[
        [
            'title'   =>'公告管理',
            'name'    =>'plugin_execute/_plugin/Notice',
            'icon'    => 'fa-envelope-o',
            'is_menu' => 1,
            'sub_menu'=>[
                [
                    'title'=>'增加公告',
                    'name' => 'Notice/Notice/index?box_type=add_notice',
                    'is_menu'=>1
                ],
                [
                    'title'=>'删除公告',
                    'name' => 'Notice/Notice/index?box_type=delete_notice',
                    'is_menu'=>1
                ],
            ]
        ]
        
    ],
];