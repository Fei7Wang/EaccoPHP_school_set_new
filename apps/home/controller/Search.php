<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2018 http://www.eacoo123.com, All rights reserved.         
// +----------------------------------------------------------------------
// | [EacooPHP] 并不是自由软件,可免费使用,未经许可不能去掉EacooPHP相关版权。
// | 禁止在EacooPHP整体或任何部分基础上发展任何派生、修改或第三方版本用于重新分发
// +----------------------------------------------------------------------
// | Author:  心云间、凝听 <981248356@qq.com>
// +----------------------------------------------------------------------
namespace app\home\controller;
use app\home\model\Users;
use app\cms\model\Posts;
use app\common\logic\User;
class Search extends Home {

    function _initialize()
    {
        parent::_initialize();

    }
    
    /**
     * 首页
     * @return [type] [description]
     * op=1 代表文章 op=0 代表用户
     */
    public function search($key,$op=1)
    {
        $this->pageInfo('首页','home');
        $this->assign('pageTitle',$key."-搜索结果");

        $userModel=new Users();
        $user=$userModel->where('nickname', 'like', "%".$key."%")->select();
        $userCount=$userModel->where('nickname', 'like', "%".$key."%")->count();

        $postModel=new Posts();
        $post=$postModel->where('title', 'like', "%".$key."%")->select();
        $postCount=$postModel->where('title', 'like', "%".$key."%")->count();

        foreach($post as $p)
        {
            $p["author"]=$postModel->getAuthorAttr("",$p);
        }

        $this->assign("userList",$user);
        $this->assign("postList",$post);
        $this->assign("key",$key);
        $this->assign("userCount",$userCount);
        $this->assign("postCount",$postCount);
        $this->assign("op",$op);
    	return $this->fetch();
    }
    
    
}