<?php

return [

    // 后台菜单及权限节点配置
    'admin_menus' =>[
        [
            'title'   =>'公司留言',
            'name'    =>'CompanyMessage',
            'icon'    => '',
            'is_menu' => 1,
            //'sort'    => 3,
            'sub_menu'=>[
                [
                    'title'=>'留言表单Form',
                    'name' => 'home/plugin/execute?_plugin=CompanyMessage&_controller=CompanyMessageForm&_action=Index',
                    'is_menu'=>1
                ],
                [
                    'title'=>'留言列表List',
                    'name' => 'home/plugin/execute?_plugin=CompanyMessage&_controller=CompanyMessageList&_action=Index',
                    'is_menu'=>1
                ],
            ]
        ]
        
    ],
];