<?php
namespace plugins\comm;

use app\common\controller\Plugin;


use app\common\logic\User;
use plugins\comm\model\Comment;
use plugins\comm\model\Reply;

class Index extends Plugin{
    /***
     *
     */
    public $hooks = [
        'ShowComment',
        'CommAndRep',
    ];
    public function install(){
        return true;
    }

    public function uninstall(){
        //删除钩子
        return true;
    }

    public function ShowComment($pid){

        $commentModel=new Comment();
        $replyModel=new Reply();

        $commList=$commentModel->where(['aid'=>$pid['pid']])->order('date desc')->select();
        foreach ($commList as $commentItem) {
            $commentItem['comm_user']=User::info($commentItem['uid']);

            $listRep=$replyModel->where(['cid'=>$commentItem['id']])->select();
            foreach ($listRep as $repItem){
                $repItem['rep_user']=User::info($repItem['uid']);
            }

            $commentItem['reply']=$listRep;
        }

        $this->assign('commList',$commList);
        return $this->fetch('comment_list');
    }
    public function CommAndRep($pid){
        $commentModel=new Comment();
        $replyModel=new Reply();

        $commList=$commentModel->where(['aid'=>$pid['pid']])->select();
        $sum=0;
        foreach ($commList as $commentItem) {
            $commentItem['comm_user']=User::info($commentItem['uid']);
            $listRep=$replyModel->where(['cid'=>$commentItem['id']])->select();
            $sum++;
            foreach ($listRep as $repItem){
                $repItem['rep_user']=User::info($repItem['uid']);
                $sum++;
            }
        }
        $this->assign('num',$sum);
        return $this->fetch('num');
    }
   
}