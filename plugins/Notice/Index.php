<?php
/**
 * Created by PhpStorm.
 * User: Yu bo
 * Date: 2018-05-10
 * Time: 17:54
 */
namespace plugins\notice;
use app\common\controller\Plugin;
/**
 *公告管理插件
 *@author Frank Yu
 *@date 2018-05-10
 */

class index extends Plugin
{
    public $hooks = [
    'Notice',
    ];
    public function install(){
        return true;
    }

    public function uninstall(){
        //删除钩子
        return true;
    }

    public function Notice()
    {
        return $this->fetch('notice_list');
    }
}