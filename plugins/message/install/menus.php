<?php

return [

    // 后台菜单及权限节点配置
    'admin_menus' =>[
        [
            'title'   =>'站内信',
            'name'    =>'plugin_execute/_plugin/message',
            'icon'    => 'fa-envelope-o',
            'is_menu' => 1,
            'sub_menu'=>[
                [
                    'title'=>'收件箱',
                    'name' => 'message/Message/index?box_type=inbox',
                    'is_menu'=>1
                ],
                [
                    'title'=>'发件箱',
                    'name' => 'message/Message/index?box_type=outbox',
                    'is_menu'=>1
                ],
            ]
        ]
        
    ],
];