<?php
// 站内信控制器       
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2017 http://www.eacoo123.com, All rights reserved.         
// +----------------------------------------------------------------------
// | [EacooPHP] 并不是自由软件,可免费使用,未经许可不能去掉EacooPHP相关版权。
// | 禁止在EacooPHP整体或任何部分基础上发展任何派生、修改或第三方版本用于重新分发
// +----------------------------------------------------------------------
// | Author:  心云间、凝听 <981248356@qq.com>
// +----------------------------------------------------------------------
namespace plugins\message\admin;

use app\home\admin\Plugin; 
use plugins\message\model\Message as MessageModel;

class Message extends Plugin {
    protected $tab_list;
    protected $messageModel;
    protected $plugin_name='message';

    function _initialize()
    {
        parent::_initialize();
        $this->messageModel = new MessageModel();
    }
    
    /**
     * 消息列表
     * @param  string $box_type [description]
     * @return [type] [description]
     * @date   2018-03-08
     * @author 心云间、凝听 <981248356@qq.com>
     */
    public function index($box_type){
        $meta_title = $box_type=='inbox' ? '收件箱':'发件箱';

        $this->assign('meta_title',$meta_title);
        $this->assign('box_type',$box_type);

        // 获取所有该用户站内信
        if ($box_type=='inbox') {//发件箱
            $msg_map['to_uid']=is_login();
        }elseif ($box_type=='outbox') {
           $msg_map['from_uid']=is_login();
        }
        $msg_map['pid']     = 0;
        $msg_map['is_read'] = 0;
        $msg_map['status']  = 1;
        //dump($msg_map);
        list($data_list,$totalCount) =$this->messageModel->getListByPage($msg_map); 
        foreach($data_list as $k=>$data){
            $data_list[$k]['fromuid_avatar']   = get_user_info($data['from_uid'])['avatar'];
            $data_list[$k]['touid_avatar']     = get_user_info($data['to_uid'])['avatar'];
            
            $data_list[$k]['fromuid_nickname'] = get_user_info($data['from_uid'])['nickname'];
            $data_list[$k]['touid_nickname']   = get_user_info($data['to_uid'])['nickname'];

            if ($box_type=='inbox') {
                $data_list[$k]['avatar']   =$data_list[$k]['fromuid_avatar'];
                $data_list[$k]['nickname'] =$data_list[$k]['fromuid_nickname'];
            } elseif ($box_type=='outbox') {
                $data_list[$k]['avatar']   =$data_list[$k]['touid_avatar'];
                $data_list[$k]['nickname'] =$data_list[$k]['touid_nickname'];
            }
            
        }
        $this->assign('message_list',$data_list);

        $inboxMessageCount  =$this->messageModel->newMessageCount(null,'inbox');
        $outboxMessageCount =$this->messageModel->newMessageCount(null,'outbox');
        
        $this->assign('inboxMessageCount',$inboxMessageCount);
        $this->assign('outboxMessageCount',$outboxMessageCount);
        return $this->fetch();
    }

    /**
     * 发送消息
     * @param  integer $from_uid [description]
     * @param  integer $type [description]
     * @return [type] [description]
     * @date   2018-03-08
     * @author 心云间、凝听 <981248356@qq.com>
     */
    public function send_message($from_uid=0,$type=1){
        $this->assign('meta_title','发送消息');
        $dataMessage=[];
        $from_uid=0;   //作者bug，此行不能删除
        if (!$from_uid) {
           $from_uid = is_login();
           
        }
        if(IS_POST && $from_uid){
           
            $to_uids = input('post.to_uids');
            $to_uids = explode(',',$to_uids);
            
            foreach ($to_uids as $key => $to_uid) {
                $data['from_uid'] = $from_uid;
                $data['to_uid']   = $to_uid;
                $data['title']    = input('post.title');
                $data['content']  = input('post.content','','htmlspecialchars_decode');
                $data['type']     = $type;
                $data['pid']      = 0;
                
                if ($from_uid===$to_uid) {
                    $this->error('不能给自己发送消息');
                }else{
                    if (!$data['content']) {
                        $this ->error('消息内容不能为空');
                    }
                    $result =$this->messageModel->sendMessage($data);
                    if (!$result) {
                        $this ->error($this->messageModel->getError());
                    }
                }
            }
            if($result){
                $this ->success('发送成功');
            }
            return;
        } else{
            $inboxMessageCount  =$this->messageModel->newMessageCount(null,'inbox');
            $outboxMessageCount =$this->messageModel->newMessageCount(null,'outbox');
            
            $this->assign('inboxMessageCount',$inboxMessageCount);
            $this->assign('outboxMessageCount',$outboxMessageCount);
            return $this->fetch();
        }
        
    }
    
    /**
     * 消息详情
     * @param  [type] $to_uid [description]
     * @return [type] [description]
     * @date   2018-03-08
     * @author 心云间、凝听 <981248356@qq.com>
     */
    function detail($to_uid){
        
        $this->assign('meta_title','消息详情');
        // 获取所有该用户站内信
                
        $data_list = $this->messageModel->where(function ($query) use($to_uid) {
                $query->where(['to_uid'=>$to_uid,'from_uid'=>is_login()]);
            })->whereOr(function ($query)use($to_uid) {
                $query->where(['to_uid'=>is_login(),'from_uid'=>$to_uid]);
            })->order('create_time asc')->limit(60)->select();
        foreach($data_list as $k=>$data){
            $data_list[$k]['fromuid_avatar']   = get_user_info($data['from_uid'])['avatar'];
            $data_list[$k]['touid_avatar']     = get_user_info($data['to_uid'])['avatar'];
            
            $data_list[$k]['fromuid_nickname'] = get_user_info($data['from_uid'])['nickname'];
            $data_list[$k]['touid_nickname']   = get_user_info($data['to_uid'])['nickname'];
        }
        $this->assign('message_list',$data_list);
        $this->assign('to_user_info',get_user_info($to_uid));

        $inboxMessageCount  = $this->messageModel->newMessageCount(null,'inbox');
        $outboxMessageCount = $this->messageModel->newMessageCount(null,'outbox');
        
        $this->assign('inboxMessageCount',$inboxMessageCount);
        $this->assign('outboxMessageCount',$outboxMessageCount);
        return $this->fetch();
        
    }
}
