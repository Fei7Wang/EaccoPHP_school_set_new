<?php
// 内容模型       
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2017 http://www.eacoo123.com, All rights reserved.         
// +----------------------------------------------------------------------
// | [EacooPHP] 并不是自由软件,可免费使用,未经许可不能去掉EacooPHP相关版权。
// | 禁止在EacooPHP整体或任何部分基础上发展任何派生、修改或第三方版本用于重新分发
// +----------------------------------------------------------------------
// | Author:  心云间、凝听 <981248356@qq.com>
// +----------------------------------------------------------------------
namespace app\home\model;

use app\common\model\Base;

class Focus extends Base {
    protected $name='focus';
    protected $autoWriteTimestamp = false;

    /**
     * 获取粉丝数
     * @param  [type] $id [用户ID]
     * @return [type] [关注人id]
     * @date   2018-01-06
     * @author 岳晓飞 <1326037045@qq.com>
     */
    public function getFansCount($id)
    {
        $fansCount=$this->where('uid_b',$id)->count();
        return $fansCount;
    }

    /**
     * 获取粉丝数
     * @param  [type] $id [用户ID]
     * @return [type] [关注人id]
     * @date   2018-01-06
     * @author 岳晓飞 <1326037045@qq.com>
     */
    public function getFocusCount($id)
    {
        $focusCount=$this->where('uid_a',$id)->count();
        return $focusCount;
    }
}