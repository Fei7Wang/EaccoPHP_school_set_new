<?php
return [
    'type'=>[
        'title'=>'仪表盘工具:',
        'type'=>'checkbox',
        'options'=>[
            'recently_reg_user' =>'最新注册用户',
            'grow_analyze'      =>'用户增长分析'
        ],
        'value'=>'recently_reg_user,grow_analyze',
    ],
];
                    