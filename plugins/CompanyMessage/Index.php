<?php
namespace plugins\CompanyMessage;
use app\common\controller\Plugin;
/**
 * 公司留言插件
 */
class Index extends Plugin{

    /**
     * @var array 插件钩子
     */
    public $hooks = [];

    /**
     * 插件安装方法
     */
    public function install(){
        return true;
    }

    /**
     * 插件卸载方法
     */
    public function uninstall(){
        return true;
    }

}
