<?php
namespace plugins\DemoExample;
use app\common\controller\Plugin;
/**
 * 演示Builder示例
 */
class Index extends Plugin{

    /**
     * @var array 插件钩子
     */
    public $hooks = [];

    /**
     * 插件安装方法
     */
    public function install(){
        return true;
    }

    /**
     * 插件卸载方法
     */
    public function uninstall(){
        return true;
    }

}
