<?php
// 站内信控制器       
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2017 http://www.eacoo123.com, All rights reserved.         
// +----------------------------------------------------------------------
// | [EacooPHP] 并不是自由软件,可免费使用,未经许可不能去掉EacooPHP相关版权。
// | 禁止在EacooPHP整体或任何部分基础上发展任何派生、修改或第三方版本用于重新分发
// +----------------------------------------------------------------------
// | Author:  心云间、凝听 <981248356@qq.com>
// +----------------------------------------------------------------------
namespace plugins\comm\controller;

use app\home\admin\Plugin;
use app\home\controller\Home;
use plugins\comm\model\Reply;
use think\Controller;
use think\Request;


class Comment extends Home {
    protected $commentModel;
    protected $plugin_name='comm';
    protected $repModel;
    function _initialize()
    {
        parent::_initialize();
        $this->commentModel = new \plugins\comm\model\Comment();
        $this->repModel=new Reply();
    }
    
    /**
     * 添加评论
     * @date   2018-07-4
     * @author 王中玉
     */
    public function addComment(){
        $content=input('content');
        $commenter_id=input('uid');
        $article_id=input('aid');
        $this->commentModel->uid=$commenter_id;
        $this->commentModel->aid=$article_id;
        $this->commentModel->content=$content;
        $this->commentModel->save();
        $this->success('评论成功',url('cms/Index/detail',['id'=>$article_id]));
    }

    /***
     * 添加回复
     */
    public function addRep(){
        $cid=input('cid');
        $article_id=input('aid');
        $uid=\is_login();
        $content=input('content');
        $this->repModel->data(['cid'=>$cid,'uid'=>$uid,'content'=>$content])->save();
        $this->success('回复成功',url('cms/Index/detail',['id'=>$article_id]));
    }
    /**
     * 赞
     */

    public function zan($id,$type)
    {

        $model=$this->commentModel->get($id);
        $model->zan=$model->zan+1;
        $model->save();

    }
    public  function cai($id,$type){
        $model=$this->commentModel->get($id);
        $model->cai=$model->cai+1;
        $model->save();
    }
}
