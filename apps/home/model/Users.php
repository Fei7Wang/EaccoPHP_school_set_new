<?php
// 内容模型       
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2017 http://www.eacoo123.com, All rights reserved.         
// +----------------------------------------------------------------------
// | [EacooPHP] 并不是自由软件,可免费使用,未经许可不能去掉EacooPHP相关版权。
// | 禁止在EacooPHP整体或任何部分基础上发展任何派生、修改或第三方版本用于重新分发
// +----------------------------------------------------------------------
// | Author:  心云间、凝听 <981248356@qq.com>
// +----------------------------------------------------------------------
namespace app\home\model;

use app\common\model\Base;

class Users extends Base {
    protected $name='users';

    /**
     * 获取用户信息
     * @param  [type] $id [用户uid]
     * @return [type] [用户全部信息]
     * @date   2018-01-06
     * @author 岳晓飞 <1326037045@qq.com>
     */
    public function getInfo($id)
    {
        $result=$this->where('uid',$id)->find();
        return $result;
    }
}
