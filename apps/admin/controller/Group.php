<?php

namespace app\admin\controller;

use app\admin\model\AuthRule as AuthRuleModel;
use app\admin\model\AuthGroup as AuthGroupModel;
use app\admin\model\AuthGroupAccess as AuthGroupAccessModel;
use app\admin\logic\AuthGroup as AuthGroupLogic;
use app\common\model\User as UserModel;
use think\Db;


class Group extends Admin
{

    protected $authRuleModel;
    protected $authGroupModel;
    protected $userModel;
    protected $authGroupLogic;

    function _initialize()
    {
        parent::_initialize();

        $this->authRuleModel  = new AuthRuleModel();
        $this->authGroupModel = new AuthGroupModel();
        $this->userModel     = new UserModel;
        //$this->authGroupLogic = new AuthGroupLogic;
    }

    /**
     * 是时候展示真正的技术了
     * @author 王中玉 <>
     */

    public function roleMember($group_id=0)
    {
       // 获取构建器列表数据
       $map = [
           'status'=>['egt',0],// 禁用和正常状态
       ];
       $group_name= model('AuthGroup')->where(['id'=>$group_id])->field('title')->find();
       $this->assign(['meta_title'=>$group_name['title']]);

       if (IS_AJAX){
           $sql='select * from eacoo_users WHERE uid in (select uid FROM eacoo_auth_group_access WHERE group_id = ? )';
           $list=  Db::query($sql,[$group_id]);
           return $list;
       }
       return $this->fetch();

    }

    /***
     * 返回不是当前角色的用户
     */
    public function notRoleMember($group_id=0)//////////////////////////////////
    {
        if (IS_AJAX){
            $sql='select * from eacoo_users WHERE uid not in (select uid FROM eacoo_auth_group_access WHERE group_id = ? )';
            $list=  Db::query($sql,[$group_id]);
            return $list;
        }
    }
    public function edit($var = null)
    {
        try {
            $uids = input('uid',false);// bug 修改 uids更改为uid 2018.4.29
            if ($uids) {
                $uid = explode(',',$uids);
            }else{
                $uid = input('uid');
            }
            
            $gid = input('param.group_id');
            if( empty($uid) ){
                throw new \Exception("参数有误", 0);
                
            }
            if(is_numeric($uid)){
                if ( is_administrator($uid) ) {
                    throw new \Exception("该用户为超级管理员", 0);
                }
                if( !$this->userModel->where(['uid'=>$uid])->find() ){
                    throw new \Exception("用户不存在", 0);
                }
            }

            if( $gid && !$this->authGroupModel->checkGroupId($gid)){
                $this->error($this->authGroupModel->error);
            }
            if ( !logic('AuthGroup')->addToGroup($uid,$gid) ){
                $this->error($this->authGroupModel->getError());
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('操作成功');
    }
    /**
     * 批量加入组
     */
    public function batchadd($model = CONTROLLER_NAME, $script = false)
    {
        $ids = input('post.uids/a');
        $groupid=input('post.group_id');
        if ( !logic('AuthGroup')->addToGroup($ids,$groupid) ){
            $this->error($this->authGroupModel->getError());
        }

        $this->success('操作成功');
    }
    /**
     * 批量从角色中删除
     */
    public function batchrm(){
        $ids = input('post.uids/a');
        $groupid=input('post.group_id');
        if ( !logic('AuthGroup')->batchRemoveFromGroup($ids,$groupid) ){
            $this->error($this->authGroupModel->getError());
        }

        $this->success('操作成功');
    }
}

