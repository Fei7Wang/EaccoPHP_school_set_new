<?php

// +----------------------------------------------------------------------
namespace app\cms\controller;
use app\home\controller\Home;

use app\cms\model\Posts as PostsModel;
use app\cms\logic\Category as CategoryLogic;
use app\common\model\Terms as TermsModel;
use app\cms\logic\Tag as TagLogic;
use app\cms\model\Postmeta as PostmetaModel;



class Article extends Home {

    protected $postModel;
    protected $termsModel;
	function _initialize()
    {
        parent::_initialize();

        $this->postModel  = new PostsModel();
        $this->termsModel = new TermsModel();
        $this->assign('category_list',CategoryLogic::getCategories());
        $this->assign('tag_list',TagLogic::getTags());
    }   



    /**
     * 新增或编辑

     */
    public function edit($id = 0){

        $title = $id>0 ? "编辑":"新增";
        $this->assign('pageTitle',$title);
        
        //修改
        if(IS_POST){
            $data = $this->request->param();
    
            $data['author_id']    = is_login();
            $data['type']         = 'post';
            $data['content']      = htmlspecialchars_decode($data['content']);
            //$data['fields']     =input('fields');
            
            //验证数据
            $this->validateData($data,'Post.edit');
            $result = $this->postModel->editData($data);
            if($result){
                $postmeta = input('param.postmeta/a');
                if (!empty($postmeta)) {
                    $post_meta_model = new PostmetaModel;
                    $meta_keys = $post_meta_model->where('post_id',$id)->column('meta_key');
                     /* 提交过来的 跟数据库中比较 不存在 删除*/
                     $del_metas = [];
                     $postmeta_keys = array_column($postmeta,'meta_key');
                     if (!empty($meta_keys)) {
                         foreach ($meta_keys as $key => $value) {
                            if(!in_array($value,$postmeta_keys)) $del_metas[] = $value; 
                        }
                     }
                     //这是元数据
                    foreach ($postmeta as $key => $val) {
                        if (!empty($val['meta_value'])) {
                            $post_meta_model->setMeta($id,$val['meta_key'],$val['meta_value']);
                        }
                        
                    }
                    //删除不存在的
                    foreach ($del_metas as $key => $value) {
                        $post_meta_model->deleteMeta($id,$value);
                    }
                }
                update_post_term($id,input('post.category_id',false));
                $this ->success($title.'成功','index/index');
            } else{
                $this ->error($this->postModel->getError());
            }

        } else{
            $this->assign('page_config',['disable_panel'=>true,'back'=>true]);
            $this->assign('meta_title',$title.'文章');

            $info = [
                'content'=>'',
                'img'=>''
            ];
            if ($id>0) {
                $info = PostsModel::get($id);
                $this->assign('category_id',get_the_category($id));
                $this->assign('tag_ids',get_the_category($id));
                $this->assign('meta_list',PostmetaModel::getMetas($id));
            } else {
                $this->assign('category_id',0);
                $this->assign('tag_ids',0);
            }
            $this->assign('info',$info);
            
            $this->assign('form_url',url('edit',['id'=>$id]));

            $this->assign('post_category',logic('Category')->getOptTerms('post_category'));
            $this->assign('post_tags',logic('Category')->getOptTerms('post_tag'));
            $this->assign('tag_id',1);//测试
            return $this->fetch();
        }
        
    }

    public function check(Type $var = null)
    {
        # code...
    }


}